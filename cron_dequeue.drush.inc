<?php
/**
 * @file
 * Drush hooks for the Cron Deqeue module.
 */

/**
 * Implements hook_drush_command().
 */
function cron_dequeue_drush_command() {
  $items = array();

  $items['cron-dequeue'] = array(
    'description' => t('Dequeue a single batch from the cron queues.'),
  );

  return $items;
}

/**
 * Drush command callback for the cron-dequeue command.
 */
function drush_cron_dequeue() {
  cron_dequeue();
}
